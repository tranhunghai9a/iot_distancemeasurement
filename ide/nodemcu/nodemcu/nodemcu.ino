1.  #include "ESPAsyncWebServer.h"
2.  #include <Wire.h>
3.   
4.  const char* ssid = "nhập tên wifi";
5.  const char* password = "nhập password";
6.   
7.  String content=""; //tạo biến string để nhận kết quả từ Arduino
8.   
9.   
10. AsyncWebServer server(80);     // tạo websever trên cổng 80
11.  
12. void setup()
13. {
14.   Serial.begin(115200); // khởi tạo serial monitor
15.   Wire.begin(D1, D2); // join i2c bus with SDA=D1 and SCL=D2 of NodeMCU 
16.   Serial.print("Setting STATION mode…");
17.  
18.   WiFi.mode(WIFI_STA);
19.   WiFi.begin(ssid, password); //dùng để thiết lập chế độ Station
20.   
21.   while(WiFi.status()!=WL_CONNECTED)
22.   {
23.     Serial.print(".");                
24.     delay(500);
25.   }
26.   
27.   Serial.print("STA IP address: ");
28.   Serial.println(WiFi.localIP());       //để in ra IP đã kết nổi ở Station Mode
29.  
30.   
31.   server.begin();
32. }
33.  
34.  
35. void loop(){
36.   // nhận tín hiểu từ arduino
37.   
38.  Wire.requestFrom(8,12); // yêu cầu Arduino(địa chỉ 8) với result 12 byte
39.  while(Wire.available()){ // đưa resultt vào string content
40.    for(int i=0;i<12;i++){
41.       byte c = Wire.read(); 
42.       content += char(c);
43.     } 
44.    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
45.    request->send(200, "text/plain", content);
46.    });
47.    delay(1000);
48.    Serial.println(content);
49.    content =""; // trả String còntent về rỗng
50.  }
51. }
