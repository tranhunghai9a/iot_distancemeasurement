1.  //D1 - A4 (SDA) #chuẩn giao tiếp I2C
2.  //D2 - A5 (SCL) 
3.  #define TRIGGER 8
4.  #define ECHO 7
5.  #define TIME_OUT 30000
6.  #include <Wire.h>
7.   
8.  char result1[8], result2[8]; // Buffer big enough for 7-character float
9.   
10. // triển khai function lấy khoảng cách cm
11. float getDistance_cm()
12. {
13.   unsigned int duration;
14.   float distance;
15.   digitalWrite(TRIGGER,0); //TRIG at low for 2 microsec
16.   delayMicroseconds(2);
17.   digitalWrite(TRIGGER,1); // TRIG at high for 5 microsec
18.   delayMicroseconds(5);
19.   digitalWrite(TRIGGER,0);
20.  
21.   duration = pulseIn(ECHO,1,TIME_OUT); // thời gian TRIG phát phản vật và thu bởi Echo
22.   distance = (duration/2/29.1545); 
23.  
24.   return distance; // khoảng cách giữa vật và thiết bị iot (cm)
25. }
26.  
27.  
28. // Function that executes whenever data is requested from master
29. void requestEvent() {
30.   Wire.write(result1);  // gửi dữ liệu qua ESP
31.   Wire.write(result2);
32. }
33.  
34. void setup()
35. {
36.   Serial.begin(115200); //khởi tạo serial monitor
37.   pinMode(TRIGGER,OUTPUT); //Pin TRIG phát
38.   pinMode(ECHO,INPUT);  // pin Echo thu
39.  
40.   //gửi tín hiêu Thông qua giao tiếp I2c
41.   Wire.begin(8);                // Join i2c với địa chỉ là 8 
42.  
43.   Wire.onRequest(requestEvent); // khởi tạo request event
44. }
45.  
46. void loop()
47. {
48.   float value = getDistance_cm();
49.   dtostrf(value, 6, 2, result1); //6 ký tự với 2 là số kí tự thập phần sau "."
50.   dtostrf(value/2.54, 6, 2, result2); //lấy khoảng cách inch
51.   Serial.println(result1); //monitor hiện kết quả cm
52.   Serial.println(result2);//monitor hiện kết quả inch
53.   delay(1000); //sau 1 giây đo lại
54. }
